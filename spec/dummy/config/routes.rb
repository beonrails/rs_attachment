Dummy::Application.routes.draw do
  resources :tweets
  resources :articles

  #mount RsPages::Engine, :at => "/pages"
  mount RsAttachment::Engine, :at => "/attachments"

  devise_for :users,
    :controllers => {
      :confirmations => 'users/confirmations',
      :registrations => 'users/registrations',
      :sessions      => 'users/sessions',
    }

  devise_scope :user do
    get 'users/registration/done' => 'users/registrations#done'
  end

  root :to => "home#index"
end
