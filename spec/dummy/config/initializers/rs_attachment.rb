# encoding: utf-8

RsAttachment.setup do |config|
  # Mime types that allowed to upload
  config.accept_mime_types = %w(image/jpeg image/jpg image/pgpeg image/gif image/png)

  # File extensions that allowed to upload
  config.accept_extensions = %w(jpg jpeg png gif)

  # Max file size to upload
  config.max_file_size = 10.megabytes

  # Min file size to upload
  config.min_file_size = 1.kilobyte

  # Number of files we can upload
  config.max_number_of_files = 20

  # Image min and max width
  config.image_width_range = (120..5000)

  # Image min and max height
  config.image_height_range = (120..5000)

  # Big thumbnail size (ImageMagic format)
  config.thumb_big_image_size = '600x460#'

  # Small thumbnail size (ImageMagic format)
  config.thumb_small_image_size = '120x120#'
end
