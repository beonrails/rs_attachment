class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.datetime :published_at
      t.boolean :hot
      t.string :author_email
      t.string :author_url
      t.string :author_phone
      t.text :article

      t.timestamps
    end
  end
end
