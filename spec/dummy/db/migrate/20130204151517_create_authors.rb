class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :name, :null => false
      t.references :tweet, :null => false
    end
  end
end
