# This migration comes from rs_attachment (originally 20121212221759)
class AddAssociationNameToAttachments < ActiveRecord::Migration
  def change
    add_column :rs_attachments_attachments, :association_name, :string, :default => nil
    add_index :rs_attachments_attachments, [:association_name, :attachable_type, :attachable_id], :name => :index_assoc
  end
end
