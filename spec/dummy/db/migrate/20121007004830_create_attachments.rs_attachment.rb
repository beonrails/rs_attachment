# This migration comes from rs_attachment (originally 20120615185034)
class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :rs_attachments_attachments do |t|
      t.string  :attachable_type,  :default => nil
      t.integer :attachable_id,    :default => 0
      t.string  :attach_uid,       :null => false
      t.string  :name,             :default => nil
      t.integer :width,            :default => nil
      t.integer :height,           :default => nil
      t.integer :size,             :null => false
      t.string  :mime_type,        :null => false
      t.boolean :image,            :default => false
      t.string  :description,      :default => nil
      t.timestamps
    end

    add_index :rs_attachments_attachments, [:attachable_type, :attachable_id], :name => 'attachable_type_attachable_id'
    add_index :rs_attachments_attachments, :height
    add_index :rs_attachments_attachments, :width
    add_index :rs_attachments_attachments, :size
    add_index :rs_attachments_attachments, :attach_uid
  end
end
