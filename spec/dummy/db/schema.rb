# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130204151517) do

  create_table "authors", :force => true do |t|
    t.string  "name",     :null => false
    t.integer "tweet_id", :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.datetime "published_at"
    t.boolean  "hot"
    t.string   "author_email"
    t.string   "author_url"
    t.string   "author_phone"
    t.text     "article"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "rs_attachments_attachments", :force => true do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id",    :default => 0
    t.string   "attach_uid",                          :null => false
    t.string   "name"
    t.integer  "width"
    t.integer  "height"
    t.integer  "size",                                :null => false
    t.string   "mime_type",                           :null => false
    t.boolean  "image",            :default => false
    t.string   "description"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "association_name"
  end

  add_index "rs_attachments_attachments", ["association_name", "attachable_type", "attachable_id"], :name => "index_assoc"
  add_index "rs_attachments_attachments", ["attach_uid"], :name => "index_rs_attachments_attachments_on_attach_uid"
  add_index "rs_attachments_attachments", ["attachable_type", "attachable_id"], :name => "attachable_type_attachable_id"
  add_index "rs_attachments_attachments", ["height"], :name => "index_rs_attachments_attachments_on_height"
  add_index "rs_attachments_attachments", ["size"], :name => "index_rs_attachments_attachments_on_size"
  add_index "rs_attachments_attachments", ["width"], :name => "index_rs_attachments_attachments_on_width"

  create_table "tweets", :force => true do |t|
    t.string   "message",    :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
