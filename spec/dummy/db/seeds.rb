# encoding: utf-8

if Rails.env.development? || Rails.env.staging?
  # Clear database
  Rake::Task["db:reset"].invoke
end

####################################################
# Create users
user = User.new do |x|
  x.email                 = "johndoe@example.com"
  x.password              = "secret"
  x.password_confirmation = "secret"
end

user.save!