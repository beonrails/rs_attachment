# encoding: utf-8

class TweetsController < ApplicationController
  respond_to :html

  def index
    @tweets = Tweet.all
  end

  def show
    @tweet = Tweet.find(params[:id])
  end

  def new
    @tweet = Tweet.new
    @tweet.images.build
    @tweet.build_avatar
    @tweet.build_author
    @tweet.author.build_photo
  end

  def edit
    @tweet = Tweet.find(params[:id])
    @tweet.images.build unless @tweet.images.any?
    @tweet.build_avatar unless @tweet.avatar
    @tweet.build_author unless @tweet.author
    @tweet.author.build_photo unless @tweet.author.photo
    # do nothing
  end

  def create
    @tweet = Tweet.new(params[:tweet])

    unless @tweet.save
      @tweet.images.build unless @tweet.images.any?
      @tweet.build_avatar unless @tweet.avatar
      @tweet.build_author unless @tweet.author
      @tweet.author.build_photo unless @tweet.author.photo
    end

    respond_with(@tweet)
  end

  def update
    @tweet = Tweet.find(params[:id])
    
    unless @tweet.update_attributes(params[:tweet])
      @tweet.images.build unless @tweet.images.any?
      @tweet.build_avatar unless @tweet.avatar
      @tweet.build_author unless @tweet.author
      @tweet.author.build_photo unless @tweet.author.photo
    end

    respond_with(@tweet)
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    respond_with(@tweet)
  end

end