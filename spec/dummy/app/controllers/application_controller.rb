# encoding: utf-8

class ApplicationController < RocketscienceController
  protect_from_forgery

  before_filter do
    unless view_context.current_page?(main_app.root_path)
      add_crumb 'Главная', view_context.main_app.root_path
    end
  end

end
