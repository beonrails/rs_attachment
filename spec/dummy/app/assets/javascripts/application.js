//= require rs_common/rs_common

// Load only necessary bootstrap stuff
//= require bootstrap-dropdown
//= require bootstrap-tab
//= require bootstrap-button
//= require bootstrap-collapse
//= require bootstrap-modal
//= require bootstrap-tooltip
//= require bootstrap-popover

// require rs_attachment/rs_attachment