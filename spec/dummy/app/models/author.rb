# encoding: utf-8

class Author < ActiveRecord::Base
  belongs_to :tweet

  attr_accessible :name
  validates :name, :presence => true

  rs_attachment :photo do
    max_number_of_files 1
    max_file_size 80.kilobytes
  end

  def to_s
    name
  end
end
