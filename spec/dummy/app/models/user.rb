# encoding: utf-8

class User < ActiveRecord::Base
  include Extensions::User::Common
  include Extensions::User::Devise
end
