# encoding: utf-8

class Tweet < ActiveRecord::Base
  attr_accessible :message, :author_attributes
  validates :message, :presence => true

  rs_attachments :images do
    max_number_of_files 10
    max_file_size 100.kilobytes
  end

  rs_attachment :avatar do
    max_number_of_files 1
    max_file_size 100.kilobytes
  end

  # У твита есть Автор, можно загрузить фото автора при создании твита
  has_one :author
  accepts_nested_attributes_for :author
  validates_associated :author

  def to_s
    message
  end
end
