//= require bootstrap-image-gallery/load-image

(function( $ ) {
  $.fn.imagePopups = function(o) {
    // options
    var o = $.extend({}, o)

    // Main object
    $.fn.ImagePopups = function(el) { this.initialize(el) }

    $.fn.ImagePopups.prototype = {

      // Initialize
      initialize: function(el) {
        var el = $(el)
        
        this.buildPopup()

        switch(el.prop('nodeName')) {
          case 'A': {
            this.process(el)
            break;
          }
          case 'IMG': {
            this.process(el.closest('a'))
            break;
          }
        }
      },

      buildPopup: function() {
        $('body').append($(' \
          <div id="modal-gallery" class="modal modal-gallery hide fade"> \
            <div class="modal-header"> \
              <a class="close" data-dismiss="modal">&times;</a> \
              <h3 class="modal-title"></h3> \
            </div> \
            <div class="modal-body"><div class="modal-image"></div></div> \
            <div class="modal-footer"> \
              <a class="btn btn-info modal-prev"><i class="icon-arrow-left icon-white"></i> Назад</a> \
              <a class="btn btn-primary modal-next">Далее <i class="icon-arrow-right icon-white"></i></a> \
              <a class="btn modal-download" target="_blank"><i class="icon-download"></i> Скачать</a> \
            </div> \
          </div>')
        )
      },

      process: function(el) {
        if (el.parent('div.gallery').length) return
        this.wrap(el.attr('rel', 'gallery'))
      },

      wrap: function(el) {
        var div = $(document.createElement('div'))
          .addClass('gallery')
          .attr('data-toggle', 'modal-gallery')
          .attr('data-target', '#modal-gallery')
        
        return el.wrap(div)
      }
    }

    return this.each(function() {
      new $.fn.ImagePopups(this)
    })
  }
})(jQuery)