# Lazy loading for Image Popups plugin
$ ->
  objects = $('a[role=gallery]')

  # Handle link to images. This creates image popups
  if objects.length
    LazyLoad.css('/assets/bootstrap-image-gallery/bootstrap-image-gallery.css')

    LazyLoad.js([
      '/assets/bootstrap-image-gallery/load-image.js',
      '/assets/bootstrap-image-gallery/bootstrap-image-gallery.js',
      '/assets/bootstrap-image-gallery/image-popups.js'], ->

      objects.imagePopups()
    )