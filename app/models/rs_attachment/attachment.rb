# encoding: utf-8

=begin
TODO: Сейчас при загрузке PDF аттачей имеют место тормоза. Нужно написать
воркюраунд для Dragonfly, чтобы он не анализировал PDF вложения.

Подробнее здесь: https://github.com/markevans/dragonfly/issues/229

TODO: Валидировать расширение файлов и сохранять его в БД

TODO НУЖЕН ГЛОБАЛЬНЫЙ ОБЪЕКТ С КОНФИГУРАЦИЕЙ
=end

module RsAttachment
  class Attachment < ActiveRecord::Base
    self.table_name = 'rs_attachments_attachments'

    IMAGES_MIME_TYPES = %w(image/jpeg image/png image/gif image/bmp image/x-xbm)
    IMAGES_MIME_REGEX = %r{^image/\w+}
    EXTENSION_REGEX   = /(\.[^.]+)\Z/i

    belongs_to      :attachable, :polymorphic => true
    file_accessor   :attach

    attr_accessible :attach, :name, :attachable_id, :attachable_type, :width, :height,
      :size, :mime_type, :image, :description, :association_name, :as => [:default, :admin]

    before_validation :set_validators

    # Determine attachment is image?
    before_create :determine_attachment_type

    # Set attach attributes
    before_create :set_attach_attributes

    default_scope order(:updated_at)

    def to_s
      attach.name
    end

  private
    
    # Determine attachment is image?
    # Do not use default attach.image? method coz its buggy and slow
    def determine_attachment_type
      is_image = IMAGES_MIME_TYPES.include?(attach.mime_type) && attach.mime_type.match(IMAGES_MIME_REGEX).present?
      write_attribute(:image, is_image)
      nil
    end

    def set_attach_attributes
      if image?
        write_attribute(:width, attach.width)
        write_attribute(:height, attach.height)
      end
      
      write_attribute(:name, attach.name)
      write_attribute(:size, attach.size)
      write_attribute(:mime_type, attach.mime_type)
      nil
    end


    def set_validators
      configuration = RsAttachment.rs_attachment_configuration[self.attachable_type.underscore][self.association_name]

      self.class.class_eval do
        validates :attach,
          :presence => { :blank => I18n.t('rs_attachment.errors.no_file') },
          :on => :create

        validates :attach, :length => {
          :maximum => configuration.max_file_size,
          :minimum => configuration.min_file_size,
          :too_long => I18n.t('rs_attachment.errors.too_big',
            :size => ActionController::Base.helpers.number_to_human_size(configuration.max_file_size)
          ),
          :too_short => I18n.t('rs_attachment.errors.too_small',
            :size => ActionController::Base.helpers.number_to_human_size(configuration.min_file_size)
          )
        }

        validates_property :format,
          :of => :attach,
          :in => configuration.accept_extensions.map(&:to_sym),
          :case_sensitive => false,
          :message => proc {
            I18n.t('rs_attachment.errors.bad_format', :expected => configuration.accept_extensions.join(', '))
          }

        validates_property :mime_type,
          :of => :attach,
          :in => configuration.accept_mime_types,
          :message => proc {
            I18n.t('rs_attachment.errors.bad_mime_type', :expected => configuration.accept_mime_types.join(', '))
          }

        validates_property :width,
          :of => :attach,
          :in => configuration.image_width_range,
          :if => :image?,
          :message => proc {
            I18n.t('rs_attachment.errors.bad_width', :expected_from => configuration.image_width_range.min, :expected_to => configuration.image_width_range.max)
          }

        validates_property :height,
          :of => :attach,
          :in => configuration.image_height_range,
          :if => :image?,
          :message => proc {
            I18n.t('rs_attachment.errors.bad_height', :expected_from => configuration.image_height_range.min, :expected_to => configuration.image_height_range.max)
          }

        # Validate upload files limit
        #validate do
        #  #not_null = RsAttachment::Attachment.arel_table[:attachable_id].not_eq(nil)
        #  records_count = RsAttachment::Attachment.where(
        #    :association_name => self.association_name,
        #    :attachable_id    => self.attachable_id,
        #    :attachable_type  => self.attachable_type
        #  ).count#.where(not_null).count
        #  if configuration.max_number_of_files == records_count
        #    errors.add(:base, "You can upload only #{configuration.max_number_of_files} files")
        #  end
        #end
      end
    end
  end
end