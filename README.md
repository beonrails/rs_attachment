# RsAttachment

Данный гем позволяет добавить поддержку загрузки файлов и изображений в приложение.

## Установка

Чтобы установить `RsAttachment` в ваше приложение, нужно:

* Подключить его в Gemfile
```ruby
gem 'rs_attachment', :git => 'git@rocketscience.it:rs_attachment.git'
```
* Инсталлировать
  ```bundle install```
* Установить в приложение
  ```rails g rs_attachment:install```

Теперь всё готово, чтобы использовать `RsAttachment` в вашем приложении.

## Использование

Например, мы хотим прикреплять к товару фотографии товара:

```ruby
class Product < ActiveRecord::Base
  # Например, у товара может быть 10 фотографий.
  # Обратите внимание, если аттачей больше 1, то нужно писать `rs_attachments`,
  # в противном случае - `rs_attachment`
  rs_attachments :photos do
    max_number_of_files 10
  end

  # и одна обложка. По-умолчанию max_number_of_files == 1
  rs_attachment :cover
end
```
Далее, в форму создания нового товара добавляем поля:

```erb
<%= f.attachments :photos %>
<%= f.attachment :cover %>
```

И в контроллере нужно модифицировать методы `create` и `update` примерно так:

```ruby
  def new
    @product.photos.build unless @product.photos.any?
    @product.build_cover unless @product.cover
  end

  def edit
    @product.photos.build unless @product.photos.any?
    @product.build_cover unless @product.cover
  end

  def create
    unless @product.save
      @product.photos.build unless @product.photos.any?
      @product.build_cover unless @product.cover
    end
  end

  def update
    unless @product.update_attributes(params[:product])
      @product.photos.build unless @product.photos.any?
      @product.build_cover unless @product.cover
    end
  end
```

Конечно, вышеперечисленный методы контроллера можно упростить до:

```ruby
  def new
    build_associations
  end

  def edit
    build_associations
  end

  def create
    build_associations unless @product.save
  end

  def update
    build_associations unless @product.update_attributes(params[:product])
  end

protected

  def build_associations
    @product.photos.build unless @product.photos.any?
    @product.build_cover unless @product.cover
  end

```

### Настройки

При установке этого гема автоматически создаётся инициалайзер `config/initializers/rs_attachment.rb`. В нём прописаны настройки по-умолчанию.

Чтобы переопределить эти настройки для конкретной модели, используйте специальный синтаксис:

```ruby
class Product < ActiveRecord::Base
  # Включаем RsAttachment
  rs_attachment :photos do
    max_file_size 1.megabyte
    min_file_size 1.kilobyte

    accept_extensions %w(jpg jpeg png gif pdf)

    accept_mime_types %w(
      image/jpeg
      image/jpg
      image/pgpeg
      image/gif
      image/png
      application/pdf
      application/vnd.pdf
      text/pdf
      application/x-pdf
    )

    image_width_range 500..1000
  end
end
```

Список настроек по-умолчанию:

| Настройка | Значение по-умолчанию | Описание |
|-----------|-----------------------|----------|
| accept_mime_types | %w(image/jpeg image/jpg image/pgpeg image/gif image/png) | Mime types that allowed to upload |
| accept_extensions | %w(jpg jpeg png gif) | File extensions that allowed to upload |
| max_file_size | 10.megabytes | Max file size to upload |
| min_file_size | 1.kilobyte | Min file size to upload |
| max_number_of_files | 1 | Number of files we can upload |
| image_width_range | (120..5000) | Image min and max width |
| image_height_range | (120..5000) | Image min and max height |
| thumb_big_image_size | '600x460#' | Big thumbnail size (ImageMagic format) |
| thumb_small_image_size | '120x120#' | Small thumbnail size (ImageMagic format) |


### Галерея Bootstrap Image Gallery

При помощи ленивой загрузки по-умолчанию в приложение подключается плагин Bootstrap Image Gallery, который обеспечивает показ картинок во всплывающих окнах. Для того, чтобы его использовать, нужно просто указать необходимую HTML разметку в коде страницы, а загрузка и инициализация плагина произойдёт автоматически. Вот примеры кода:

```html
<!-- Ссылка на изображение. В этом случае нужно явно указывать role="gallery", чтобы картинка открылась в новом окне -->
<a role="gallery" href="http://example.com/original.jpg">View Image</a>

<!-- Ссылка на картинку, с картинкой внутри ссылки. -->
<a href="http://example.com/original.jpg" role="gallery">
  <img src="http://example.com/thumbnail.jpg">
</a>
```

Чтобы вывести галерею с возможностью перехода по картинкам вперед-назад, нужно применять следующую разметку:

(ЭТО ЕЩЁ НЕ РАБОТАЕТ)

```html
<div class="gallery" data-toggle="modal-gallery" data-target="#modal-gallery">
  <a href="http://example.com/original1.jpg">
    <img src="http://example.com/thumbnail1.jpg">
  </a>

  <a href="http://example.com/original2.jpg">
    <img src="http://example.com/thumbnail2.jpg">
  </a>
</div>
```

### View-хелперы

Для вывода множества аттачей используйте хелпер `attachments`:

```erb
<%= attachments(@vacancy.photos) %>
```

Для вывода единственного аттача используйте хелпер `attachment`:

```erb
<%= attachment(@resume.photo) %>
```

### Rake-таски

По-умолчанию в геме идёт пока один таск, который позволяет очищать неиспользуемые картинки Dragonfly, например - он очищает тамбнейлы, оставляя только оригинальные изображения. Это полезно, когда вы поменяли размер изображений на сайте и старые размеры более не используются. Так же этот таск удаляет картинки, записи для которых отсутствуют в таблице images.

Запустить таск можно так: `rake dragonfly:cleanup` 