RsAttachment::Engine.routes.draw do
  resources :attachments, :only => [:index, :create, :destroy] do
    get 'attachable/:attachable_type/:association_name/(:attachable_id)' => 'attachments#attachable',
      :on => :collection,
      :as => :attachable
  end

  put '/attachments' => 'attachments#create', :as => :attachments
  root :to => 'attachments#index'
end
