# encoding: utf-8
require 'rs_attachment/configuration'
require 'rs_attachment/association'

module RsAttachment
  mattr_accessor :rs_attachment_configuration

  module ActiveRecordExtensions
    extend ActiveSupport::Concern

    included do
      RsAttachment.rs_attachment_configuration ||= {}.with_indifferent_access
    end

    module ClassMethods
      def rs_attachment(association_name, &config)
        class_eval do
          association = Association.new(base_class, association_name, :has_one, &config)
          RsAttachment.rs_attachment_configuration[name.underscore] ||= {}.with_indifferent_access
          RsAttachment.rs_attachment_configuration[name.underscore][association_name] = association.configuration
        end
      end

      def rs_attachments(association_name, &config)
        class_eval do
          association = Association.new(base_class, association_name, :has_many, &config)
          RsAttachment.rs_attachment_configuration[name.underscore] ||= {}.with_indifferent_access
          RsAttachment.rs_attachment_configuration[name.underscore][association_name] = association.configuration
        end
      end
    end
  end
end