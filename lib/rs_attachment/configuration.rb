# encoding: utf-8
require 'ostruct'

module RsAttachment
  class Configuration
    attr_accessor :settings

    def initialize(&config)
      @settings = OpenStruct.new(default_settings)

      if block_given?
        self.instance_eval(&config)
      end

      @settings
    end

    def method_missing(m, *args, &block)
      if @settings.respond_to?(m)
        # Get setting
        if args.empty?
          @settings.send(m)
        # Set setting and return result
        else
          @settings.send("#{m}=", *args)
        end
      else
        raise "Undefined setting #{m}"
      end
    end

    def default_settings
      {
        # Number of files we can upload
        :max_number_of_files    => RsAttachment.max_number_of_files,

        # Default max size of file
        :max_file_size          => RsAttachment.max_file_size,

        # Default min size of file
        :min_file_size          => RsAttachment.min_file_size,

        # Extensions that we able to upload
        :accept_extensions      => RsAttachment.accept_extensions,

        # Mime-types that we able to upload
        :accept_mime_types      => RsAttachment.accept_mime_types,

        # Image min and max width
        :image_width_range      => RsAttachment.image_width_range,

        # Image min and max height
        :image_height_range     => RsAttachment.image_height_range,

        # To show image in popup at upload form
        :thumb_big_image_size   => RsAttachment.thumb_big_image_size,

        # To show image in uploaded files list at upload form
        :thumb_small_image_size => RsAttachment.thumb_small_image_size,
      }
    end
  end
end