# encoding: utf-8
require 'rs_attachment/attachment_helper'

module RsAttachment
  # Набор view helpers
  module ActionViewExtensions
    def attachment(attachment, options = {})
      RsAttachment::AttachmentHelper.new(self, attachment, options).to_s
    end

    def attachments(attachments, options = {})
      content_tag :ul, :class => :attachments do
        items = []
        attachments.each do |attachment|
          items << content_tag(:li, attachment(attachment))
        end

        concat(raw(items.join))
      end
    end    
  end
end