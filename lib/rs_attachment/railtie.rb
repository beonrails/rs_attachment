require 'rs_attachment'
require 'rails'

module RsAttachment
  class Railtie < Rails::Railtie
    # AR extension
    initializer 'rs_attachment.active_record' do
      if defined?(ActiveRecord::Base)
        require 'rs_attachment/active_record_extensions'
        ActiveSupport.on_load(:active_record) do
          ActiveRecord::Base.send :include, RsAttachment::ActiveRecordExtensions
        end
      end
    end

    # ActionView extension
    initializer "rs_attachment.action_view" do |app|
      require 'rs_attachment/action_view_extensions'
      ActionView::Base.send :include, RsAttachment::ActionViewExtensions
    end
  end
end