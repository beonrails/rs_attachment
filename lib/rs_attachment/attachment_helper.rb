# encoding: utf-8

module RsAttachment
  class AttachmentHelper
    attr_accessor :template, :attachment, :options

    delegate :content_tag, :image_tag, :raw, :concat, :number_to_human_size, :to => :@template
    delegate :url, :thumb, :name, :size, :to => '@attachment.attach', :prefix => 'attach'

    def initialize(template, attachment, options = {})
      @template   = template
      @attachment = attachment
      @options    = options.reverse_merge!({
        :size => RsAttachment.thumb_small_image_size
      })
    end

    def to_s
      wrap_attachment do
        self.attachment.image? ? image_attachment : file_attachment
      end
    end

  private

    def wrap_attachment(&block)
      class_name = self.attachment.image? ? 'image-attachment thumbnail' : 'file-attachment'
      content_tag :div, block.call, :class => "#{class_name} rs-attachment"
    end

    def image_attachment
      thumb = attach_thumb(@options[:size])

      content_tag :a, :href => attach_url, :role => :gallery do
        image_tag thumb.url,
          :alt    => '',
          :width  => thumb.width,
          :height => thumb.height
      end
    end

    def file_attachment
      link = content_tag :a, attach_name,
        :href  => attach_url,
        :title => I18n.t('rs_attachment.download_caption')

      size = content_tag :span, raw("&nbsp;(#{number_to_human_size(attach_size)})")
      raw(link + size)
    end
  end
end