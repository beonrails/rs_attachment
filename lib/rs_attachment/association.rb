module RsAttachment
  class Association
    attr_accessor :klass, :association_name, :association_type, :configuration

    def initialize(klass, association_name, association_type, &config)
      @klass            = klass
      @association_name = association_name
      @association_type = association_type
      @configuration    = RsAttachment::Configuration.new(&config)

      create_association!
    end

  protected

    def create_association!
      @klass.class_eval %{
        attr_accessible :#{@association_name}_attributes

        #{@association_type} :#{@association_name}, #{association_options}

        accepts_nested_attributes_for :#{@association_name},
          :reject_if     => proc { |x| x[:attach].blank? },
          :allow_destroy => true
      }
    end

    def association_options
      options = {
        :as         => :attachable,
        :dependent  => :destroy,
        :class_name => '::RsAttachment::Attachment',
        :conditions => { :association_name => @association_name }
      }

      if @association_type == :has_many
        options[:limit] = @configuration.max_number_of_files
      end

      options
    end
  end
end