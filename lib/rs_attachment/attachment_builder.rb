# encoding: utf-8

module RsAttachment
  class AttachmentBuilder
    delegate :object, :template, :to => :@builder

    def initialize(builder, association_name, options)
      @builder          = builder
      @options          = options
      @association_name = association_name
    end

    def render
      template.render 'rs_attachment/attachments/partials/attachment',
        :builder          => @builder,
        :options          => options,
        :association_name => @association_name,
        :multiple         => multiple?
    end

  private

    def association_type
      @builder.object.class.base_class.reflect_on_association(@association_name).macro
    end

    def multiple?
      association_type == :has_many
    end

    def classname
      @builder.object.class.base_class.name.underscore
    end

    def configuration
      RsAttachment.rs_attachment_configuration[classname][@association_name]
    end

    def options
      @options.reverse_merge!(configuration.marshal_dump)
    end
  end
end