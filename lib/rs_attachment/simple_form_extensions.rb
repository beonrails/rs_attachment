# encoding: utf-8
require 'simple_form'

module RsAttachment
  module SimpleFormExtensions
    extend ActiveSupport::Concern

    def attachment(association_name, options = {})
      RsAttachment::AttachmentBuilder.new(self, association_name, options).render
    end

    def attachments(*args)
      attachment(*args)
    end
  end
end

SimpleForm::FormBuilder.send :include, RsAttachment::SimpleFormExtensions