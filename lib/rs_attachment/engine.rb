require "delayed_job_active_record"

module RsAttachment
  class Engine < ::Rails::Engine
    isolate_namespace RsAttachment
  end
end
