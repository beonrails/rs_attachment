require "rs_attachment/engine"
require "rs_attachment/simple_form_extensions"
require "rs_attachment/attachment_builder"

module RsAttachment
  # Mime types that allowed to upload
  mattr_accessor :accept_mime_types
  @@accept_mime_types = :accept_mime_types

  # File extensions that allowed to upload
  mattr_accessor :accept_extensions
  @@accept_extensions = :accept_extensions

  # Max file size to upload
  mattr_accessor :max_file_size
  @@max_file_size = :max_file_size

  # Min file size to upload
  mattr_accessor :min_file_size
  @@min_file_size = :min_file_size

  # Number of files we can upload
  mattr_accessor :max_number_of_files
  @@max_number_of_files = :max_number_of_files

  # Image min and max width
  mattr_accessor :image_width_range
  @@image_width_range = :image_width_range

  # Image min and max height
  mattr_accessor :image_height_range
  @@image_height_range = :image_height_range

  # Big thumbnail size (ImageMagic format)
  mattr_accessor :thumb_big_image_size
  @@thumb_big_image_size = :thumb_big_image_size

  # Small thumbnail size (ImageMagic format)
  mattr_accessor :thumb_small_image_size
  @@thumb_small_image_size = :thumb_small_image_size

  def self.setup
    yield self
  end
end


if defined?(Rails::Railtie)
  require 'rs_attachment/railtie'
end