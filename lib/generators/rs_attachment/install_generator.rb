# encoding: utf-8
require 'rails/generators'

# TODO
# Не копировать миграцию uploads

module RsAttachment
  module Generators
    class InstallGenerator < ::Rails::Generators::Base #:nodoc:
      source_root File.expand_path("../templates", __FILE__)

      desc "Install RsAttachment into app"
      def install
        setup_initializer
        setup_dragonfly_initializer
        setup_stylesheet_assets
        setup_javascript_assets
        setup_precompiled_assets

        route 'mount RsAttachment::Engine, :at => "/attachments"'
        generate 'rs_attachment:locales'
        rake 'rs_attachment:install:migrations'
        rake 'db:migrate'
      end

    protected

      def app_name
        ::Rails.application.class.parent_name.to_s
      end

      def setup_initializer #:nodoc:
        copy_file 'config/initializers/rs_attachment.rb', 'config/initializers/rs_attachment.rb'
      end

      def setup_dragonfly_initializer #:nodoc:
        copy_file 'config/initializers/dragonfly.rb', 'config/initializers/dragonfly.rb'
      end

      def setup_stylesheet_assets #:nodoc:
        append_file "app/assets/stylesheets/application.css.scss" do
          %Q(\n@import "rs_attachment/rs_attachment";)
        end
      end

      def setup_javascript_assets #:nodoc:
        jsfile = ::Rails.root.join('app', 'assets', 'javascripts', 'application.js')
        coffeefile = ::Rails.root.join('app', 'assets', 'javascripts', 'application.js.coffee')

        append_file jsfile, %Q(\n//= require rs_attachment/rs_attachment) if File.exists?(jsfile)
        append_file coffeefile, %Q(\n#= require rs_attachment/rs_attachment) if File.exists?(coffeefile)
      end

      def setup_precompiled_assets #:nodoc:
        replace = "config.assets.precompile += %w( "
        replacement = "config.assets.precompile += %w( bootstrap-image-gallery/image-popups.js bootstrap-image-gallery/bootstrap-image-gallery.js bootstrap-image-gallery/bootstrap-image-gallery.css"
        gsub_file 'config/environments/production.rb', replace, replacement
        gsub_file 'config/environments/staging.rb', replace, replacement
      end

    end
  end
end