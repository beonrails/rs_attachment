require 'rails/generators'

module RsAttachment
  module Generators
    class LocalesGenerator < Rails::Generators::Base #:nodoc:
      source_root File.expand_path("../../../../config/locales", __FILE__)
      desc "Copy rs_attachment locales to your application's locales."

      def copy_views
        directory 'rs_attachment', 'config/locales/rs_attachment'
      end
    end
  end
end
