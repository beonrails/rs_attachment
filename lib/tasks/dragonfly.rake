# /home/masterurala/htdocs/shared/system/dragonfly/production/2012/02/17
namespace :dragonfly  do
  desc "Cleanup thumbnails and unusued images"
  task :cleanup => :environment do
    path_prefix = Rails.root.join("public/system/dragonfly/#{Rails.env}")

    existing_images = {}
    Image.find_each do |image|
      existing_images["#{path_prefix}/#{image.image_uid}"] = true
    end

    counter = 0
    Dir["#{path_prefix}/**/**/**/*"].each do |file|
      if !existing_images.has_key?(file) && File.file?(file)
      	File.unlink(file)
        puts "Removed: #{file}"
        counter += 1
      end
    end

    puts "Removed total: #{counter} files"
  end
end