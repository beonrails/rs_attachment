module Dragonfly
  module Processing
    class WatermarkProcessor
      include Dragonfly::Configurable

      configurable_attr :use_filesystem, true

      def with_watermark(temp_object)
        path  = ::Rails.root.join('app', 'assets', 'images', 'watermarks', 'watermark.png')
        mark  = MiniMagick::Image.open(path)
        image = MiniMagick::Image.read(temp_object.data)

        image.composite mark do |c|
          c.gravity 'NorthEast'
        end
      end

    end
  end
end 