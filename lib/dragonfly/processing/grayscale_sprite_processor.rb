module Dragonfly
  module Processing
    class GrayscaleSpriteProcessor
      include Dragonfly::Configurable

      configurable_attr :use_filesystem, true

      # convert boyarsky.jpg -colorspace Gray boyarsky.jpg  +append -quality 75 boyarsky.jpg
      def gsprite(temp_object)
        image    = MiniMagick::Image.read(temp_object.data)
        argument = "#{temp_object.path} -colorspace Gray #{temp_object.path} +append -quality 80 #{image.path}"

        image.run(MiniMagick::CommandBuilder.new("convert", argument))
        image
      end

    end
  end
end