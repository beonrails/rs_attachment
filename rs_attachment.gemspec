$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rs_attachment/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rs_attachment"
  s.version     = RsAttachment::VERSION
  s.authors     = ["Andrew Kozlov"]
  s.email       = ["demerest@gmail.com"]
  s.homepage    = "http://gitlab.rocketscience.it/rs_attachment"
  s.description = "Attachments (files and images) for Rocketscience apps"
  s.summary     = s.description

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 3.2.6"
  s.add_dependency 'jquery-rails', '2.1.3'
  s.add_dependency 'simple_form', '2.0.3'
  s.add_dependency 'delayed_job_active_record', '0.3.3'
  s.add_dependency 'fineuploader-rails', '3.0.0'

  # Dragonfly and dependencies
  s.add_dependency 'dragonfly', '0.9.12'
  s.add_dependency 'dragonfly-minimagick', '0.0.1'
  s.add_dependency 'rack-cache', '1.2'
 
  s.add_development_dependency "sqlite3"
end
